<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous" />
    <!-- AOS -->
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

    <title>Portofolio</title>
  </head>
  <body id="home">
    <link rel="stylesheet" href="style.css" />

    <!-- Navbar 1 (Tombol navigasi) -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary shadow-sm">
      <div class="container">
        <a class="navbar-brand" href="#"><b>Welcome to My Site</b></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav ms-auto">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="#home"><b>Home</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#about"><b>About Me</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#projects"><b>Jawaban UTS</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#contact"><b>Contact</b></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- akhir Navbar 1 (Tombol navigasi) -->

    <!-- Jumbo tron  -->
    <section class="jumbotron text-center">
      <img src="img/foto.jpeg" width="250" class="rounded-circle" />
      <h1 class="nama" data-aos="flip-up" data-aos-duration="1500">APRILA PARMA REGINA<br>NPM : 10118984<br>Kelas : 4KA06</h1>
      <p class="lead">=======================================================================================</p>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill="#ffffff"
          fill-opacity="1"
          d="M0,192L48,
                176C96,160,192,128,288,133.3C384,139,480,181,576,218.7C672,256,768,288,864,272C960,256,1056,192,1152,181.3C1248,171,
                1344,213,1392,234.7L1440,256L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,
                576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"
        ></path>
      </svg>
    </section>
    <!-- akhir Jumbo tron  -->

    <!-- about -->
    <section id="about">
      <div class="container">
        <div class="row text-center">
          <div class="col">
            <h2>About Me</h2>
          </div>
        </div>
        <div class="row justify-content-center fs-5">
          <div class="col-md-4" data-aos="fade-right" data-aos-duration="2000" data-aos-delay="100">
            <p><br><br>Saya Aprila Parma Regina dengan NPM 10118984 Kelas 4KA06. Saya lahir pada tanggal 23 April 2000 di Jakarta. Saya dulu bersekolah di SMA Budhi Warman 2 Jakarta.
            </p>
          </div>
          <div class="col-md-4" data-aos="fade-left" data-aos-duration="2000" data-aos-delay="200">
            <p><br><br>Website ini di buat untuk memenuhi tugas Ujian Tengah Semester matakuliah Pemrograman Berbasis Web pada perkuliahan semester 7.
            </p>
          </div>
        </div>
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill="#b3d1ff"
          fill-opacity="1"
          d="M0,192L34.3,202.7C68.6,213,137,
    235,206,229.3C274.3,224,343,192,411,202.7C480,213,549,267,617,277.3C685.7,288,754,256,823,229.3C891.4,203,960,181,1029,170.7C1097.1,160,
    1166,160,1234,170.7C1302.9,181,1371,203,1406,213.3L1440,224L1440,320L1405.7,320C1371.4,320,1303,320,1234,320C1165.7,320,1097,320,1029,320C960,
    320,891,320,823,320C754.3,320,686,320,617,320C548.6,320,480,320,411,320C342.9,320,274,320,206,320C137.1,320,69,320,34,320L0,320Z"
        ></path>
      </svg>
    </section>

    <section id="projects">
      <div class="container">
        <div class="row text-center mb-3">
          <div class="col">
            <h3>Jawaban UTS</h3>
          </div>
        </div>
        <div class="row text-center mb-3">
         
          <form name='biodata' method='post' action='tutor.html'>
          <pre>
<br><br><b>NPM</b>    : <input type='number' name='npm'><p id='val_npm'>* Hanya boleh di-isi dengan angka</p>
<b>Nama</b>    : <input type='text' name='nama' ><p id='val_nama'>* Masukkan Nama Mahasiswa</p>
<b>Agama</b>   : <select name='agama'>
            <option>Islam
            <option>Hindu
            <option>Budha
            <option>Kristen              
            <option>Konghucu        
            </select><p>* Pilih Agama</p>
          </pre>
        <input type='button' onClick='terimainput()' value='Simpan'>
        <input style="margin-bottom: 20px" type='reset' value='Ulangi'>
        </form>

<table border='1' id='tabelinput'>
<tr>
  <td>NPM</td>
  <td>NAMA</td>
  <td>AGAMA</td>
</tr>
</table>

        </div>
      </div>

      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill="#ffffff"
          fill-opacity="1"
          d="M0,288L48,277.3C96,267,192,245,288,
            245.3C384,245,480,267,576,256C672,245,768,203,864,197.3C960,192,1056,224,1152,229.3C1248,235,1344,213,1392,202.7L1440,192L1440,320L1392,
            320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"
        ></path>
      </svg>
    </section>

    <!-- contact -->

    <section id="contact">
      <div class="container">
        <div class="row text-center">
          <div class="col">
            <h2>Contact Me</h2>
          </div>
        </div>
      </div>

      <section id="other">
        <div class="container">
          <div class="row text-center">
              <div class="col">
                <!-- <h2>Sosial Media </h2> -->
              </div>
              <div class="row justify-content-center fs-4">
                <div class="col">
                  <a href="https://www.facebook.com/parma.regi.35"> <img class="image" src="img/facebook.png" width="100px" style="margin: 1px;padding: 0px; color:white;" /></a> <h3>Facebook</h3>
                </div>
              <div class="col">
                <a href="https://www.instagram.com/parma.regi/"> <img class="image"  src="img/instagram.png" width="100px" style="margin: 1px;padding: 0px; color:white;" /></a> <h3>Instagram</h3>
              </div>
              <div class="col">
              <a href="https://gitlab.com/ParmaRegi"><img class="image" src="img/gitlab.png" width="100px" style="margin: 1px;padding: 0px; color:white;" /> </a> <h3>Gitlab</h3>
            </div>
            </div>
        </div> 
      </section>   

      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill="#b3d1ff"
          fill-opacity="1"
          d="M0,192L40,170.7C80,149,160,107,240,122.7C320,139,400,213,480,229.3C560,245,640,203,720,176C800,149,880,139,960,154.7C1040,171,1120,213,1200,229.3C1280,245,1360,235,1400,229.3L1440,224L1440,320L1400,320C1360,320,1280,320,1200,320C1120,320,1040,320,960,320C880,320,800,320,720,320C640,320,560,320,480,320C400,320,320,320,240,320C160,320,80,320,40,320L0,320Z"
        ></path>
      </svg>
    </section>

    <!-- akhir contact -->

    <!-- Navbar 3 (footer) -->

    <footer class="bg-light text-dark text-center pb-3"> 
      <p>Created By <a href="https://gitlab.com/ParmaRegi" class="text-dark fw-bold"> Aprila Parma Regina </a></p>
    </footer>
    <!-- akhir Navbar 3 (footer) -->

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

    <!-- AOS -->
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
      AOS.init();
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
    <!-- Sambungin JS -->
    <script src="javascript.js"></script>
    
    <!-- bagian Header -->

    <!-- bagian isi -->
    <!-- bagian footer -->
  </body>
</html>
